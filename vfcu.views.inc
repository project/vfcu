<?php
/**
 * @file
 * Register the plugin with views.
 */

/**
 * Implements hook_views_data().
 */
function vfcu_views_data() {

  $data['views']['custom_unrestricted'] = [
    'title' => t('Custom text unrestricted'),
    'help' => t('Provide custom text without restrictions (for site architects).'),
    'field' => [
      'id' => 'custom_unrestricted',
      'click sortable' => FALSE
    ]
  ];

  return $data;
}

?>
