<?php

namespace Drupal\vfcu\Plugin\views\field;

use Drupal\views\Plugin\views\field\Custom;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Twig\Environment;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("custom_unrestricted")
 */
class CustomUnrestricted extends Custom {
  
  /**
   *
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['acknowledge_unsafe'] = [
      'default' => FALSE
    ];
    return $options;
  }
  
  /**
   *
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
    /* add acknowledgment to further explain the plugin */
    $form['acknowledge_unsafe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I understand the <b>Text</b> is unfiltered and may contain unsafe values and understand the risks of displaying it on the site.'),
      '#default_value' => $this->options['acknowledge_unsafe']
    ];
    
    $desc = $this->t('The text to display for this field. You may enter data from this view as per the "Replacement patterns" below. You may include <a href="@twig_docs">Twig</a>, tokens, or HTML tags and attributes.', [
      '@twig_docs' => 'https://twig.symfony.com/doc/' . Environment::MAJOR_VERSION . '.x'
    ]);
    
    $form['alter']['text']['#description'] = $desc;
  }
  
  /**
   * Local filter method to allow for swapping in/out filterAdmin().
   *
   * @param $text Unsanitized
   *        string.
   *        
   * @return string
   */
  protected function filter($text) {
    if ($this->options['acknowledge_unsafe']) {
      return $this->globalTokenReplace($text);
    }
    else {
      return Xss::filterAdmin($this->globalTokenReplace($text));
    }
  }
  
  /**
   * Replaces Views' tokens in a given string.
   *
   * @param $text Unsanitized
   *        string with possible tokens.
   * @param $tokens Array
   *        of token => replacement_value items.
   *        
   *        return string
   */
  protected function viewsTokenReplace($text, $tokens) {
    if (!strlen($text)) {
      // No need to run filterAdmin on an empty string.
      return '';
    }
    if (empty($tokens)) {
      return $this->filter($text);
    }
    
    $twig_tokens = [];
    foreach ($tokens as $token => $replacement) {
      // Twig wants a token replacement array stripped of curly-brackets.
      // Some Views tokens come with curly-braces, others do not.
      // @todo: https://www.drupal.org/node/2544392
      if (strpos($token, '{{') !== FALSE) {
        // Twig wants a token replacement array stripped of curly-brackets.
        $token = trim(str_replace([
          '{{',
          '}}'
        ], '', $token));
      }
      
      // Check for arrays in Twig tokens. Internally these are passed as
      // dot-delimited strings, but need to be turned into associative arrays
      // for parsing.
      if (strpos($token, '.') === FALSE) {
        // We need to validate tokens are valid Twig variables. Twig uses the
        // same variable naming rules as PHP.
        // @see http://php.net/manual/language.variables.basics.php
        assert(preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $token) === 1, 'Tokens need to be valid Twig variables.');
        $twig_tokens[$token] = $replacement;
      }
      else {
        $parts = explode('.', $token);
        $top = array_shift($parts);
        assert(preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $top) === 1, 'Tokens need to be valid Twig variables.');
        $token_array = [
          array_pop($parts) => $replacement
        ];
        foreach (array_reverse($parts) as $key) {
          // The key could also be numeric (array index) so allow that.
          assert(is_numeric($key) || preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $key) === 1, 'Tokens need to be valid Twig variables.');
          $token_array = [
            $key => $token_array
          ];
        }
        if (!isset($twig_tokens[$top])) {
          $twig_tokens[$top] = [];
        }
        $twig_tokens[$top] += $token_array;
      }
    }
    
    if ($twig_tokens) {
      // Use the unfiltered text for the Twig template, then filter the output.
      // Otherwise, Xss::filterAdmin could remove valid Twig syntax before the
      // template is parsed.
      
      $build = [
        '#type' => 'inline_template',
        '#template' => $text,
        '#context' => $twig_tokens,
        '#post_render' => [
          function ($children, $elements) {
            return $this->filter($children);
          }
        ]
      ];
      
      // Currently you cannot attach assets to tokens with
      // Renderer::renderPlain(). This may be unnecessarily limiting. Consider
      // using Renderer::executeInRenderContext() instead.
      // @todo: https://www.drupal.org/node/2566621
      return (string) $this->getRenderer()->renderPlain($build);
    }
    else {
      return $this->filter($text);
    }
  }
  
}
