CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Views Unrestricted Custom Field module provides an unrestricted views field (for site architects).

Using the **Custom Text** views field to combine values in a view is a powerful technique for building websites. However, it doesn't
permit full HTML. Full HTML is available only via templates&mdash;too slow and cumbersome for the rapid design experience clients expect.
This view field plugin extends the **Custom Text** field and allows you to display HTML as entered. In addition, global tokens such
as `[site:name]` are processed.

This is intended for client brochure sites where content is entered by devs and marketers. This plugin is not intended for a community
site or at least not with content entered by the community. See [here](https://www.drupal.org/project/views/issues/877886) and 
[here](https://www.drupal.org/project/views/issues/853880) to find out more about the history of the issues surrounding the concepts
of this plugin.

Unless you need unrestricted HTML, you should use the core **Custom Text** views field. The Drupal recommendation for adding
unrestricted HTML to your site is to use templates.

See also:

 * https://www.drupal.org/project/drupal/issues/3019171
 * https://www.drupal.org/project/views/issues/853880
 * https://www.drupal.org/project/drupal/issues/3022643
 * https://www.drupal.org/project/views/issues/877886
 * https://www.drupal.org/project/drupal/issues/2654962
 * https://drupal.stackexchange.com/questions/253978/how-do-i-allow-full-html-while-rendering-a-view 
 * https://www.amazeelabs.com/en/journal/views-field-rewriting-view-modes 


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 1. In a view, add a field and select **Custom text unrestricted**. 
 2. Enter a value in the **Text** field.
 3. Acknowledge that you understand the risks by checking the allow unsafe values checkbox. 

Note: Leaving the allow unsafe values checkbox unchecked sends the field through the normal filtering done by **Custom Text**, which
turns the benefits and risks of this plugin off. Global tokens will still be processed either way.

You can test the plugin by entering a value like the one below. The resulting HTML should include the unmodified HTML attributes 
as well as rendered tokens and Twig syntax.

    Title: <em>{{ title }}</em> Site name is: <em>[site:name]</em> Today's date: <em>{{ 'now'|date('d M Y G:i a') }}</em>
    <div class="my-class" data-azat-an_in_delay="1000" data-azat-an_hidden="before_in" data-azat-an_in="fadeInUp">
      Card content
      <hr />
      <button>Read more button</button> 
    </div>


MAINTAINERS
-----------

Current maintainers:

 * David S. McMeans (davidmcmeans) https://www.drupal.org/u/davidmcmeans 

